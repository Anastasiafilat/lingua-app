package by.itstep.linguaapp;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;


    @Test
    public void testCreate(){
        //given
        UserEntity user=new UserEntity();
        user.setId(1);
        user.setName("Bob");
        user.setEmail("bob@gmail.com");
        user.setPassword("nas567tya");
        user.setRole(UserRole.ADMIN);
        user.setPhone("+375 29 1182360");
        user.setCountry("BY");
        //when
        userRepository.save(user);
        //then
    }

    @Test
    @Transactional
    public void testUpdate(){
        //given

        //when
        List<UserEntity> found =userRepository.findAll();
        System.out.println("FOUND: "+found);
        //then
    }

    @Test
    @Transactional
    public void findByEmail(){
        //given

        //when
        UserEntity found =userRepository.findByEmail("bob@gmail.com");
        System.out.println("FOUND: "+found);
        //then
    }

    @Test
    @Transactional
    public void findAllAdmins(){
        //given

        //when
        List<UserEntity> founds =userRepository.findAllAdmins();
        System.out.println("FOUND: "+founds);

        List<UserEntity> found =userRepository.findAllByEMailDomain("gmail.com");
        System.out.println("FOUND: "+found);
        //then
    }

    @Test
    @Transactional
    public void deleteAllAdmins(){
        //given

        //when
     userRepository.deleteAllAdmins();

        //then
    }
}



