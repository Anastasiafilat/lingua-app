package by.itstep.linguaapp.service;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

public class TestService {

    @Autowired
    private UserRepository userRepository;

    public void create (UserEntity user){
        userRepository.save(user);
    }
}
