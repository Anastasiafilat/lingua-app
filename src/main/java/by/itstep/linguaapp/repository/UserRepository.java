package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    UserEntity findByEmail(String email);

    @Query(value="SELECT * FROM users WHERE role='ADMIN'", nativeQuery = true)
    List<UserEntity> findAllAdmins();

    @Query(value="SELECT * FROM users WHERE email like %:d", nativeQuery = true)
    List<UserEntity> findAllByEMailDomain(@Param("d") String domain);

    @Modifying
    @Query(value="DELETE FROM users WHERE role='ADMIN'", nativeQuery = true)
    void deleteAllAdmins();

}
