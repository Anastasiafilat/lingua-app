package by.itstep.linguaapp.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="answer")
public class AnswerEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name="body")
    private String body;

    @Column(name="correct")
    private Boolean correct;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private QuestionEntity question;

}
