package by.itstep.linguaapp.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
public class UserEntity {

    @Id@GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column (name="name")
    private String  name;

    @Column (name="email")
    private String email;

    @Column (name="country")
    private String country;

    @Column (name="password")
    private String password;

    @Enumerated (value=EnumType.STRING)
    @Column (name="role")
    private UserRole role;

    @Column (name="phone")
    private String phone;
}
